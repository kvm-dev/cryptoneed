package ru.mykvm.cryptoneed.ui.pair

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.google.android.material.bottomappbar.BottomAppBar
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.main_fragment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.PairDetails
import ru.mykvm.cryptoneed.data.model.InfoPairData
import ru.mykvm.cryptoneed.data.model.SimplyData
import ru.mykvm.cryptoneed.data.model.retrofit.PairSettingsRetrofitImpl
import ru.mykvm.cryptoneed.databinding.PairSettingsFragmentBinding
import ru.mykvm.cryptoneed.ui.cabinet.Cabinet
import ru.mykvm.cryptoneed.ui.cabinet.CabinetFragment
import ru.mykvm.cryptoneed.ui.faq.FaqFragment
import ru.mykvm.cryptoneed.ui.historyselector.HistorySelectorFragment
import ru.mykvm.cryptoneed.ui.login.afterTextChanged
import ru.mykvm.cryptoneed.ui.menu.BottomNavigationDrawerFragment
import ru.mykvm.cryptoneed.ui.payment.PaymentFragment
import ru.mykvm.cryptoneed.ui.support.SupportFragment
import ru.mykvm.cryptoneed.ui.version.VersionFragment
import ru.mykvm.cryptoneed.viewmodels.PairSettingsViewModel

class PairSettingsFragment : Fragment() {
    private lateinit var viewModel: PairSettingsViewModel
    private val pairData="pairData"
    private lateinit var userID:String
    private lateinit var pairName:String
    private var _binding: PairSettingsFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var UpdateDataResponse:String
    private var botStartOrStop: Int = 1
    private var botManualOrAuto: Int = 1
    //datas
    lateinit var busd_sum:String
    lateinit var price_buy:String
    lateinit var price_sell:String
    lateinit var delta_buy:String
    lateinit var delta_sell:String
    lateinit var trade_mode:String
    lateinit var pair_status:String
    companion object {
        fun newInstance() = PairSettingsFragment()
        private var isMain = true
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = PairSettingsFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val bundle = arguments
        if (bundle != null) {
            val details = bundle.getParcelable<PairDetails>(pairData)
            if (details != null) {
                userID = details.userID
                pairName = details.pairName
                if (pairName.contains("ADA")) {
                    pairName = "ada"
                } else if (pairName.contains("ALICE")) {
                    pairName = "alice"
                } else if (pairName.contains("ATOM")) {
                    pairName = "atom"
                } else if (pairName.contains("BNB")) {
                    pairName = "bnb"
                } else if (pairName.contains("BTC")) {
                    pairName = "btc"
                } else if (pairName.contains("CELR")) {
                    pairName = "celr"
                } else if (pairName.contains("DODO")) {
                    pairName = "dodo"
                } else if (pairName.contains("DOGE")) {
                    pairName = "doge"
                } else if (pairName.contains("DOT")) {
                    pairName = "dot"
                } else if (pairName.contains("ETH")) {
                    pairName = "eth"
                } else if (pairName.contains("FOR")) {
                    pairName = "for"
                } else if (pairName.contains("GALA")) {
                    pairName = "gala"
                } else if (pairName.contains("POND")) {
                    pairName = "pond"
                } else if (pairName.contains("SOL")) {
                    pairName = "sol"
                } else if (pairName.contains("WIN")) {
                    pairName = "win"
                }
            }
        }
        //warning
        val warningMessage = binding.warnings
        warningMessage.visibility = View.INVISIBLE
        //other elements
        val pairIcon = binding.pairIconSettings
        val pairSettingsName = binding.pairTitleSettings
        val tradeQuantity = binding.tradeQuantity
        val pairMode = binding.pairMode
        val manualBuyPrice = binding.manualBuyPrice
        val manualSellPrice = binding.manualSellPrice
        val downDelta = binding.downDelta
        val autoPercentBenefit = binding.autoPercentBenefit
        val startStopBot = binding.startStopBot
        val buttonPairSettingsCancel = binding.buttonPairSettingsCancel
        val buttonPairSettingsSave = binding.buttonPairSettingsSave
        //not need in this scope
        val settingsPairView = binding.settingsPairView
        val warnings = binding.warnings
        val buttonToVip = binding.buttonToVip
        //check errors
        viewModel = ViewModelProvider(this, PairViewModelFactory())
            .get(PairSettingsViewModel::class.java)
        viewModel.pairFormState.observe(viewLifecycleOwner, Observer {
            val pairState = it ?: return@Observer
            // disable login button unless both username / password is valid
            buttonPairSettingsSave.isEnabled = pairState.isDataValid
            if (pairState.tradeQuantityError != null) {
                tradeQuantity.error = getString(pairState.tradeQuantityError)
            }
            if (pairState.manualBuyPriceError != null) {
                manualBuyPrice.error = getString(pairState.manualBuyPriceError)
            }

            if (pairState.manualSellPriceError != null) {
                manualSellPrice.error = getString(pairState.manualSellPriceError)
            }

            if (pairState.downDeltaError != null) {
                downDelta.error = getString(pairState.downDeltaError)
            }

            if (pairState.autoPercentBenefitError != null) {
                autoPercentBenefit.error = getString(pairState.autoPercentBenefitError)
            }
        })
        viewModel.pairResult.observe(viewLifecycleOwner, Observer {
            val pairResult = it ?: return@Observer

            if (pairResult.error != null) {
                showPairUpdateFailed(pairResult.error)
            }
            if (pairResult.success != null) {
                updateUiWithData(pairResult.success)
            }
            activity?.setResult(Activity.RESULT_OK)
        })
        //change mode
        pairMode.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                botManualOrAuto = 1;
                manualBuyPrice.isEnabled = false
                manualSellPrice.isEnabled = false
                downDelta.isEnabled = true
                autoPercentBenefit.isEnabled = true

            }else{
                botManualOrAuto = 2;
                manualBuyPrice.isEnabled = true
                manualSellPrice.isEnabled = true
                downDelta.isEnabled = false
                autoPercentBenefit.isEnabled = false
            }
        }
        //change status
        startStopBot.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                botStartOrStop = 2;

            }else{
                botStartOrStop = 1;
            }
        }
        //check errors
            tradeQuantity.afterTextChanged {
                tradeQuantity.text.toString().toIntOrNull()
                manualBuyPrice.text.toString().toDoubleOrNull()
                manualSellPrice.text.toString().toDoubleOrNull()
                downDelta.text.toString().toIntOrNull()
                autoPercentBenefit.text.toString().toIntOrNull()
                if(tradeQuantity.text.isNullOrEmpty())
                {
                    tradeQuantity.setText(resources.getString(R.string.default_zero))
                }
                if(manualBuyPrice.text.isNullOrEmpty())
                {
                    manualBuyPrice.setText(resources.getString(R.string.min_double))
                }
                if(manualSellPrice.text.isNullOrEmpty())
                {
                    manualSellPrice.setText(resources.getString(R.string.min_double))
                }
                if(downDelta.text.isNullOrEmpty())
                {
                    downDelta.setText(resources.getString(R.string.default_zero))
                }
                if(autoPercentBenefit.text.isNullOrEmpty())
                {
                    autoPercentBenefit.setText(resources.getString(R.string.default_zero))
                }
                else{
                viewModel.pairDataChanged(
                    tradeQuantity.text.toString().toInt(),
                    manualBuyPrice.text.toString().toDouble(),
                    manualSellPrice.text.toString().toDouble(),
                    downDelta.text.toString().toInt(),
                    autoPercentBenefit.text.toString().toInt()
                )
            }
            }
            manualBuyPrice.afterTextChanged {
                tradeQuantity.text.toString().toIntOrNull()
                manualBuyPrice.text.toString().toDoubleOrNull()
                manualSellPrice.text.toString().toDoubleOrNull()
                downDelta.text.toString().toIntOrNull()
                autoPercentBenefit.text.toString().toIntOrNull()
                if(tradeQuantity.text.isNullOrEmpty())
                {
                    tradeQuantity.setText(resources.getString(R.string.default_zero))
                }
                if(manualBuyPrice.text.isNullOrEmpty())
                {
                    manualBuyPrice.setText(resources.getString(R.string.min_double))
                }
                if(manualSellPrice.text.isNullOrEmpty())
                {
                    manualSellPrice.setText(resources.getString(R.string.min_double))
                }
                if(downDelta.text.isNullOrEmpty())
                {
                    downDelta.setText(resources.getString(R.string.default_zero))
                }
                if(autoPercentBenefit.text.isNullOrEmpty())
                {
                    autoPercentBenefit.setText(resources.getString(R.string.default_zero))
                }
                else {
                    viewModel.pairDataChanged(
                        tradeQuantity.text.toString().toInt(),
                        manualBuyPrice.text.toString().toDouble(),
                        manualSellPrice.text.toString().toDouble(),
                        downDelta.text.toString().toInt(),
                        autoPercentBenefit.text.toString().toInt()
                    )
                }
            }
            manualSellPrice.afterTextChanged {
                tradeQuantity.text.toString().toIntOrNull()
                manualBuyPrice.text.toString().toDoubleOrNull()
                manualSellPrice.text.toString().toDoubleOrNull()
                downDelta.text.toString().toIntOrNull()
                autoPercentBenefit.text.toString().toIntOrNull()
                if(tradeQuantity.text.isNullOrEmpty())
                {
                    tradeQuantity.setText(resources.getString(R.string.default_zero))
                }
                if(manualBuyPrice.text.isNullOrEmpty())
                {
                    manualBuyPrice.setText(resources.getString(R.string.min_double))
                }
                if(manualSellPrice.text.isNullOrEmpty())
                {
                    manualSellPrice.setText(resources.getString(R.string.min_double))
                }
                if(downDelta.text.isNullOrEmpty())
                {
                    downDelta.setText(resources.getString(R.string.default_zero))
                }
                if(autoPercentBenefit.text.isNullOrEmpty())
                {
                    autoPercentBenefit.setText(resources.getString(R.string.default_zero))
                }
                else {
                    viewModel.pairDataChanged(
                        tradeQuantity.text.toString().toInt(),
                        manualBuyPrice.text.toString().toDouble(),
                        manualSellPrice.text.toString().toDouble(),
                        downDelta.text.toString().toInt(),
                        autoPercentBenefit.text.toString().toInt()
                    )
                }
            }
        downDelta.afterTextChanged {
            tradeQuantity.text.toString().toIntOrNull()
            manualBuyPrice.text.toString().toDoubleOrNull()
            manualSellPrice.text.toString().toDoubleOrNull()
            downDelta.text.toString().toIntOrNull()
            autoPercentBenefit.text.toString().toIntOrNull()
            if(tradeQuantity.text.isNullOrEmpty())
            {
                tradeQuantity.setText(resources.getString(R.string.default_zero))
            }
            if(manualBuyPrice.text.isNullOrEmpty())
            {
                manualBuyPrice.setText(resources.getString(R.string.min_double))
            }
            if(manualSellPrice.text.isNullOrEmpty())
            {
                manualSellPrice.setText(resources.getString(R.string.min_double))
            }
            if(downDelta.text.isNullOrEmpty())
            {
                downDelta.setText(resources.getString(R.string.default_zero))
            }
            if(autoPercentBenefit.text.isNullOrEmpty())
            {
                autoPercentBenefit.setText(resources.getString(R.string.default_zero))
            }
            else {
                viewModel.pairDataChanged(
                    tradeQuantity.text.toString().toInt(),
                    manualBuyPrice.text.toString().toDouble(),
                    manualSellPrice.text.toString().toDouble(),
                    downDelta.text.toString().toInt(),
                    autoPercentBenefit.text.toString().toInt()
                )
            }
        }
        autoPercentBenefit.afterTextChanged {
            tradeQuantity.text.toString().toIntOrNull()
            manualBuyPrice.text.toString().toDoubleOrNull()
            manualSellPrice.text.toString().toDoubleOrNull()
            downDelta.text.toString().toIntOrNull()
            autoPercentBenefit.text.toString().toIntOrNull()
            if(tradeQuantity.text.isNullOrEmpty())
            {
                tradeQuantity.setText(resources.getString(R.string.default_zero))
            }
            if(manualBuyPrice.text.isNullOrEmpty())
            {
                manualBuyPrice.setText(resources.getString(R.string.min_double))
            }
            if(manualSellPrice.text.isNullOrEmpty())
            {
                manualSellPrice.setText(resources.getString(R.string.min_double))
            }
            if(downDelta.text.isNullOrEmpty())
            {
                downDelta.setText(resources.getString(R.string.default_zero))
            }
            if(autoPercentBenefit.text.isNullOrEmpty())
            {
                autoPercentBenefit.setText(resources.getString(R.string.default_zero))
            }
            else {
                viewModel.pairDataChanged(
                    tradeQuantity.text.toString().toInt(),
                    manualBuyPrice.text.toString().toDouble(),
                    manualSellPrice.text.toString().toDouble(),
                    downDelta.text.toString().toInt(),
                    autoPercentBenefit.text.toString().toInt()
                )
            }
        }
            sendServerRequest(userID, pairName)
        //update data on server
        buttonPairSettingsSave.setOnClickListener {
            (activity as Cabinet).sendServerRequest(pairName,tradeQuantity.text.toString().toInt(), manualBuyPrice.text.toString().toDouble(), manualSellPrice.text.toString().toDouble(), downDelta.text.toString().toInt(), autoPercentBenefit.text.toString().toInt(), userID,  botManualOrAuto, botStartOrStop)
        }
        buttonToVip.setOnClickListener {
            goToPayment()
        }
        buttonPairSettingsCancel.setOnClickListener {
            goToPairList()
        }
            return root
        }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }
    private fun Fragment.toast(string: String?) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).apply {
            setGravity(Gravity.BOTTOM, 0, 250)
            show()
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
             setBottomAppBar(view)
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_bottom_bar, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.faq -> goToFaq()
            R.id.history -> checkHistory()
            R.id.menu_support -> goToSupport()
            R.id.menu_payment -> goToPayment()
            R.id.menu_update -> checkVersion()
            R.id.menu_web -> goToWeb()
            android.R.id.home -> {
                activity?.let {
                    BottomNavigationDrawerFragment().show(it.supportFragmentManager, "tag")
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
    private fun setBottomAppBar(view: View) {
        val context = activity as Cabinet
        context.setSupportActionBar(view.findViewById(R.id.bottom_app_bar))
        bottom_app_bar.navigationIcon =
            ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
        setHasOptionsMenu(true)
        fab.setOnClickListener {
            if (isMain) {
                isMain = false
                bottom_app_bar.navigationIcon = null
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
                fab.setImageDrawable(
                    ContextCompat.getDrawable(context,
                    R.drawable.ic_baseline_home_24
                ))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar_other_screen)
            } else {
                PairSettingsFragment.isMain = true
                bottom_app_bar.navigationIcon =
                    ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
                fab.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cn))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar)
                //go to Pair List
                goToPairList()
            }
        }
    }
    //get data from server
    private fun sendServerRequest(userID: String, pairName: String) {
        val pairSettingsRetrofitImpl: PairSettingsRetrofitImpl = PairSettingsRetrofitImpl()
        pairSettingsRetrofitImpl.getRequest().getPairSettingsData(userID, pairName).enqueue(object :
            Callback<InfoPairData> {
            override fun onResponse(
                call: Call<InfoPairData>,
                response: Response<InfoPairData>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    renderData(response.body(), null)
                } else {
                    renderData(null, Throwable("Ответ от сервера пустой"))
                }
            }
            override fun onFailure(call: Call<InfoPairData>, t: Throwable) {
                renderData(null, t)
            }
        })
    }
    private fun renderData(dataModel: InfoPairData?, error: Throwable?) {
        if (dataModel == null || error != null) {
            //error
            var error: String? = error?.message
        } else {
            busd_sum = dataModel.busd_sum.toString()
            price_buy = dataModel.price_buy.toString()
            price_sell = dataModel.price_sell.toString()
            delta_buy = dataModel.delta_buy.toString()
            delta_sell = dataModel.delta_sell.toString()
            trade_mode = dataModel.trade_mode.toString()
            pair_status = dataModel.pair_status.toString()
            //check pair status
            val settingsPaiView = binding.settingsPairView
            val warnings = binding.warnings
            if(pair_status.contains("0"))
            {
                warnings.visibility = View.VISIBLE
                settingsPaiView.visibility = View.INVISIBLE
           }
            else
            {
                warnings.visibility = View.INVISIBLE
                settingsPaiView.visibility = View.VISIBLE
            }
            //reinitialise
            val tradeQuantity = binding.tradeQuantity
            val pairMode = binding.pairMode
            val manualBuyPrice = binding.manualBuyPrice
            val manualSellPrice = binding.manualSellPrice
            val downDelta = binding.downDelta
            val autoPercentBenefit = binding.autoPercentBenefit
            val startStopBot = binding.startStopBot
            tradeQuantity.setText(busd_sum)
            manualBuyPrice.setText(price_buy)
            manualSellPrice.setText(price_sell)
            downDelta.setText(delta_buy)
            autoPercentBenefit.setText(delta_sell)
            if(trade_mode.contains("1")){
                botManualOrAuto = 1
                pairMode.isChecked = true
            }
            else if(trade_mode.contains("2")){
                botManualOrAuto = 2
                pairMode.isChecked = false
            }
            if(pair_status.contains("1")){
                botStartOrStop = 1
                startStopBot.isChecked = false
            }
            else if(pair_status.contains("2")){
                botStartOrStop = 1
                startStopBot.isChecked = true
            }
            //change icon and pair
            val pairIcon = binding.pairIconSettings
            val pairSettingsName = binding.pairTitleSettings
            if (pairName.contains("ada")) {
                pairSettingsName.setText(getString(R.string.pair_ada))
                pairIcon.setImageResource(R.drawable.ic_ada)
            } else if (pairName.contains("alice")) {
                pairSettingsName.setText(getString(R.string.pair_alice))
                pairIcon.setImageResource(R.drawable.ic_alice)
            }else if (pairName.contains("atom")) {
                pairSettingsName.setText(getString(R.string.pair_atom))
                pairIcon.setImageResource(R.drawable.ic_atom)
            }else if (pairName.contains("bnb")) {
                pairSettingsName.setText(getString(R.string.pair_bnb))
                pairIcon.setImageResource(R.drawable.ic_bnb)
            }else if (pairName.contains("btc")) {
                pairSettingsName.setText(getString(R.string.pair_btc))
                pairIcon.setImageResource(R.drawable.ic_btc)
            }else if (pairName.contains("celr")) {
                pairSettingsName.setText(getString(R.string.pair_celr))
                pairIcon.setImageResource(R.drawable.ic_celr)
            }else if (pairName.contains("dodo")) {
                pairSettingsName.setText(getString(R.string.pair_dodo))
                pairIcon.setImageResource(R.drawable.ic_dodo)
            } else if (pairName.contains("doge")) {
                pairSettingsName.setText(getString(R.string.pair_doge))
                pairIcon.setImageResource(R.drawable.ic_doge)
            }else if (pairName.contains("dot")) {
                pairSettingsName.setText(getString(R.string.pair_dot))
                pairIcon.setImageResource(R.drawable.ic_dot)
            }else if (pairName.contains("eth")) {
                pairSettingsName.setText(getString(R.string.pair_eth))
                pairIcon.setImageResource(R.drawable.ic_eth)
            }else if (pairName.contains("for")) {
                pairSettingsName.setText(getString(R.string.pair_for))
                pairIcon.setImageResource(R.drawable.ic_for)
            }else if (pairName.contains("gala")) {
                pairSettingsName.setText(getString(R.string.pair_gala))
                pairIcon.setImageResource(R.drawable.ic_gala)
            }else if (pairName.contains("pond")) {
                pairSettingsName.setText(getString(R.string.pair_pond))
                pairIcon.setImageResource(R.drawable.ic_pond)
            }else if (pairName.contains("sol")) {
                pairSettingsName.setText(getString(R.string.pair_sol))
                pairIcon.setImageResource(R.drawable.ic_sol)
            }else if (pairName.contains("win")) {
                pairSettingsName.setText(getString(R.string.pair_win))
                pairIcon.setImageResource(R.drawable.ic_win)
            }
        }
    }
    private fun showPairUpdateFailed(@StringRes errorString: Int) {
        Toast.makeText(activity, errorString, Toast.LENGTH_SHORT).show()
    }
    private fun updateUiWithData(model: SimplyData) {
        val botHasBeenUpdated = getString(R.string.bot_config_ok)
        // TODO : initiate successful update settins of bot
        if(UpdateDataResponse.isNullOrEmpty() || UpdateDataResponse.contains("error"))
        {
            Toast.makeText(activity, getString(R.string.update_settings_server_error), Toast.LENGTH_SHORT).show()
        }
        else
        {
            Toast.makeText(
                activity,
                "$botHasBeenUpdated",
                Toast.LENGTH_LONG
            ).show()
            val cabinetFragment = CabinetFragment()
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction?.replace(R.id.container, cabinetFragment)
            transaction?.commit()
        }
    }
    private fun checkHistory()
    {
        val historySelectorFragment = HistorySelectorFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, historySelectorFragment)
        transaction?.commit()
    }
    private fun goToSupport()
    {
        val supportFragment = SupportFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, supportFragment)
        transaction?.commit()
    }
    private fun goToFaq()
    {
        val faqFragment = FaqFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, faqFragment)
        transaction?.commit()
    }
    private fun goToPayment()
    {
        val paymentFragment = PaymentFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, paymentFragment)
        transaction?.commit()
    }
    private fun checkVersion()
    {
        val versionFragment = VersionFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, versionFragment)
        transaction?.commit()
    }
    private fun goToWeb()
    {
        val uris = Uri.parse("https://cryptoneed.ru/")
        val intents = Intent(Intent.ACTION_VIEW, uris)
        val b = Bundle()
        b.putBoolean("new_window", true)
        intents.putExtras(b)
        activity?.startActivity(intents)
    }
    private fun goToPairList()
    {
        val cabinetFragment = CabinetFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, cabinetFragment)
        transaction?.commit()
    }
    }