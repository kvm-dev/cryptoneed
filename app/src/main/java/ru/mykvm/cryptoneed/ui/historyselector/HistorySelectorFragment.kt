package ru.mykvm.cryptoneed.ui.historyselector

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomappbar.BottomAppBar
import kotlinx.android.synthetic.main.main_fragment.*
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.HistoryDetails
import ru.mykvm.cryptoneed.databinding.HistorySelectorFragmentBinding
import ru.mykvm.cryptoneed.ui.cabinet.Cabinet
import ru.mykvm.cryptoneed.ui.cabinet.CabinetFragment
import ru.mykvm.cryptoneed.ui.faq.FaqFragment
import ru.mykvm.cryptoneed.ui.history.HistoryFragment
import ru.mykvm.cryptoneed.ui.menu.BottomNavigationDrawerFragment
import ru.mykvm.cryptoneed.ui.payment.PaymentFragment
import ru.mykvm.cryptoneed.ui.support.SupportFragment
import ru.mykvm.cryptoneed.ui.version.VersionFragment
import ru.mykvm.cryptoneed.viewmodels.HistorySelectorViewModel

class HistorySelectorFragment : Fragment() {
    private var _binding: HistorySelectorFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var userID:String
    private lateinit var pair:String
    private val pairData="pairData"
    private lateinit var viewModel: HistorySelectorViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = HistorySelectorFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val cabinet: Cabinet? = activity as Cabinet?
        userID = cabinet?.getUserID().toString()
        val historyFragment = HistoryFragment()
        val bundle = Bundle()
        val transaction = cabinet?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, historyFragment)
        fun goToHistory(pair: String)
        {
            var details = HistoryDetails(userID!!, pair)
            bundle.putParcelable(pairData, details)
            historyFragment.arguments = bundle
            transaction?.commit()
        }
        binding.adaPair.setOnClickListener {
            pair="ada"
            goToHistory(pair)
        }
        binding.alicePair.setOnClickListener {
            pair="alice"
            goToHistory(pair)
        }
        binding.atomPair.setOnClickListener {
            pair="atom"
            goToHistory(pair)
        }
        binding.bnbPair.setOnClickListener {
            pair="bnb"
            goToHistory(pair)
        }
        binding.btcPair.setOnClickListener {
            pair="btc"
            goToHistory(pair)
        }
        binding.celrPair.setOnClickListener {
            pair="celr"
            goToHistory(pair)
        }
        binding.dodoPair.setOnClickListener {
            pair="dodo"
            goToHistory(pair)
        }
        binding.dogePair.setOnClickListener {
            pair="doge"
            goToHistory(pair)
        }
        binding.dotPair.setOnClickListener {
            pair="dot"
            goToHistory(pair)
        }
        binding.ethPair.setOnClickListener {
            pair="eth"
            goToHistory(pair)
        }
        binding.forPair.setOnClickListener {
            pair="for"
            goToHistory(pair)
        }
        binding.galaPair.setOnClickListener {
            pair="gala"
            goToHistory(pair)
        }
        binding.pondPair.setOnClickListener {
            pair="pond"
            goToHistory(pair)
        }
        binding.solPair.setOnClickListener {
            pair="sol"
            goToHistory(pair)
        }
        binding.winPair.setOnClickListener {
            pair="win"
            goToHistory(pair)
        }
        return root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //pair list from menu
        setBottomAppBar(view)
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_bottom_bar, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.faq -> goToFaq()
            R.id.history -> toast(resources.getString(R.string.you_are_here))
            R.id.menu_support -> goToSupport()
            R.id.menu_payment -> goToPayment()
            R.id.menu_update -> checkVersion()
            R.id.menu_web -> goToWeb()
            android.R.id.home -> {
                activity?.let {
                    BottomNavigationDrawerFragment().show(it.supportFragmentManager, "tag")
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
    private fun setBottomAppBar(view: View) {
        val context = activity as Cabinet
        context.setSupportActionBar(view.findViewById(R.id.bottom_app_bar))
        bottom_app_bar.navigationIcon =
            ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
        setHasOptionsMenu(true)
        fab.setOnClickListener {
            if (HistorySelectorFragment.isMain) {
                HistorySelectorFragment.isMain = false
                bottom_app_bar.navigationIcon = null
                // bottom_app_bar.navigationIcon = ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
                fab.setImageDrawable(
                    ContextCompat.getDrawable(context,
                        R.drawable.ic_baseline_home_24
                    ))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar_other_screen)
            } else {
                HistorySelectorFragment.isMain = true
                bottom_app_bar.navigationIcon =
                    ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
                fab.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cn))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar)
                //go to Pair List
                val cabinetFragment = CabinetFragment()
                val transaction = activity?.supportFragmentManager?.beginTransaction()
                transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                transaction?.replace(R.id.container, cabinetFragment)
                transaction?.commit()
            }
        }
    }
    private fun checkHistory()
    {
        val historySelectorFragment = HistorySelectorFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, historySelectorFragment)
        transaction?.commit()
    }
    private fun goToSupport()
    {
        val supportFragment = SupportFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, supportFragment)
        transaction?.commit()
    }
    private fun goToFaq()
    {
        val faqFragment = FaqFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, faqFragment)
        transaction?.commit()
    }
    private fun goToPayment()
    {
        val paymentFragment = PaymentFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, paymentFragment)
        transaction?.commit()
    }
    private fun checkVersion()
    {
        val versionFragment = VersionFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, versionFragment)
        transaction?.commit()
    }
    private fun goToWeb()
    {
        val uris = Uri.parse("https://cryptoneed.ru/")
        val intents = Intent(Intent.ACTION_VIEW, uris)
        val b = Bundle()
        b.putBoolean("new_window", true)
        intents.putExtras(b)
        activity?.startActivity(intents)
    }
    private fun Fragment.toast(string: String?) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).apply {
            setGravity(Gravity.BOTTOM, 0, 250)
            show()
        }
    }
    companion object {
        fun newInstance() = FaqFragment()
        private var isMain = true
    }
}