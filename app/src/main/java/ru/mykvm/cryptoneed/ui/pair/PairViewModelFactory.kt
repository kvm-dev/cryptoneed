package ru.mykvm.cryptoneed.ui.pair

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.mykvm.cryptoneed.data.PairDataSource
import ru.mykvm.cryptoneed.data.PairRepository
import ru.mykvm.cryptoneed.viewmodels.PairSettingsViewModel

class PairViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PairSettingsViewModel::class.java)) {
            return PairSettingsViewModel(
                pairRepository = PairRepository(
                    dataSource = PairDataSource()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}