package ru.mykvm.cryptoneed.ui.login

data class LoginFormState(
    val usernameError: Int? = null,
    val userPasswordError: Int? = null,
    val isDataValid: Boolean = false
)