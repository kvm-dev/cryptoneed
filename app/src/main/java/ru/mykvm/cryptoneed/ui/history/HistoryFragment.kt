package ru.mykvm.cryptoneed.ui.history

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomappbar.BottomAppBar
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.history_fragment.*
import kotlinx.android.synthetic.main.main_fragment.bottom_app_bar
import kotlinx.android.synthetic.main.main_fragment.fab
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.HistoryDetails
import ru.mykvm.cryptoneed.data.adapters.HistoryRecyclerAdapter
import ru.mykvm.cryptoneed.data.model.HistoryServices
import ru.mykvm.cryptoneed.data.model.common.CommonHistory
import ru.mykvm.cryptoneed.data.model.retrofit.interfaces.RetrofitHistory
import ru.mykvm.cryptoneed.databinding.HistoryFragmentBinding
import ru.mykvm.cryptoneed.ui.cabinet.Cabinet
import ru.mykvm.cryptoneed.ui.cabinet.CabinetFragment
import ru.mykvm.cryptoneed.ui.faq.FaqFragment
import ru.mykvm.cryptoneed.ui.historyselector.HistorySelectorFragment
import ru.mykvm.cryptoneed.ui.menu.BottomNavigationDrawerFragment
import ru.mykvm.cryptoneed.ui.payment.PaymentFragment
import ru.mykvm.cryptoneed.ui.support.SupportFragment
import ru.mykvm.cryptoneed.ui.version.VersionFragment
import ru.mykvm.cryptoneed.viewmodels.HistoryViewModel

class HistoryFragment : Fragment() {

    private lateinit var userID:String
    private lateinit var pair:String
    private val pairData="pairData"
    private var _binding: HistoryFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var mHistory: RetrofitHistory
    private lateinit var layoutManagerHistory: LinearLayoutManager
    lateinit var adapterHistory: HistoryRecyclerAdapter
    private lateinit var dialog: AlertDialog
    private lateinit var userIDFromCabinet: String
    private lateinit var viewModel: HistoryViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = HistoryFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val bundle = arguments
        if (bundle != null) {
            val details = bundle.getParcelable<HistoryDetails>(pairData)
            if (details != null) {
                userID = details.userID
                pair = details.pair
            }
        }
        mHistory = CommonHistory.retrofitHistory
        binding.recyclerViewHistory.setHasFixedSize(true)
        layoutManagerHistory = LinearLayoutManager(activity)
        binding.recyclerViewHistory.layoutManager = layoutManagerHistory
        dialog = SpotsDialog.Builder().setCancelable(true).setContext(activity).build()
        //send request to server
        getAllHistoryList()
        return root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBottomAppBar(view)
        //pair list
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_bottom_bar, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.faq -> goToFaq()
            R.id.history -> checkHistory()
            R.id.menu_support -> goToSupport()
            R.id.menu_payment -> goToPayment()
            R.id.menu_update -> checkVersion()
            R.id.menu_web -> goToWeb()
            android.R.id.home -> {
                activity?.let {
                    BottomNavigationDrawerFragment().show(it.supportFragmentManager, "tag")
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
    private fun setBottomAppBar(view: View) {
        val context = activity as Cabinet
        context.setSupportActionBar(view.findViewById(R.id.bottom_app_bar))
        bottom_app_bar.navigationIcon =
            ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
        setHasOptionsMenu(true)
        fab.setOnClickListener {
            if (HistoryFragment.isMain) {
                HistoryFragment.isMain = false
                bottom_app_bar.navigationIcon = null
                // bottom_app_bar.navigationIcon = ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
                fab.setImageDrawable(
                    ContextCompat.getDrawable(context,
                    R.drawable.ic_baseline_home_24
                ))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar_other_screen)
            } else {
                HistoryFragment.isMain = true
                bottom_app_bar.navigationIcon =
                    ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
                fab.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cn))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar)
                //go to Pair List
                val cabinetFragment = CabinetFragment()
                val transaction = activity?.supportFragmentManager?.beginTransaction()
                transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                transaction?.replace(R.id.container, cabinetFragment)
                transaction?.commit()
            }
        }
    }
    private fun Fragment.toast(string: String?) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).apply {
            setGravity(Gravity.BOTTOM, 0, 250)
            show()
        }
    }
    companion object {
        fun newInstance() = CabinetFragment()
        private var isMain = true
    }
    private fun getAllHistoryList() {
        dialog.show()
        mHistory.getHistoryList(userID, pair).enqueue(object :
            Callback<MutableList<HistoryServices>> {
            override fun onFailure(call: Call<MutableList<HistoryServices>>, t: Throwable) {
            }
            override fun onResponse(call: Call<MutableList<HistoryServices>>, response: Response<MutableList<HistoryServices>>) {
                adapterHistory = activity?.let { HistoryRecyclerAdapter(it.baseContext, response.body() as MutableList<HistoryServices>) }!!
                if(response.body().isNullOrEmpty()) {
                    val selectorFragment = HistorySelectorFragment()
                    val transaction = activity?.supportFragmentManager?.beginTransaction()
                    transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction?.replace(R.id.container, selectorFragment)
                    transaction?.commit()

                    Toast.makeText(context, resources.getString(R.string.history_not_found), Toast.LENGTH_SHORT).apply {
                        setGravity(Gravity.BOTTOM, 0, 250)
                        show()
                    }
                }
                adapterHistory.notifyDataSetChanged()
                recyclerViewHistory.adapter = adapterHistory
                dialog.dismiss()
            }
        })
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    private fun goToSupport()
    {
        val supportFragment = SupportFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, supportFragment)
        transaction?.commit()
    }
    private fun goToFaq()
    {
        val faqFragment = FaqFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, faqFragment)
        transaction?.commit()
    }
    private fun goToPayment()
    {
        val paymentFragment = PaymentFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, paymentFragment)
        transaction?.commit()
    }
    private fun checkVersion()
    {
        val versionFragment = VersionFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, versionFragment)
        transaction?.commit()
    }
    private fun goToWeb()
    {
        val uris = Uri.parse("https://cryptoneed.ru/")
        val intents = Intent(Intent.ACTION_VIEW, uris)
        val b = Bundle()
        b.putBoolean("new_window", true)
        intents.putExtras(b)
        activity?.startActivity(intents)
    }
    private fun checkHistory()
    {
        val historySelectorFragment = HistorySelectorFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, historySelectorFragment)
        transaction?.commit()
    }
}