package ru.mykvm.cryptoneed.ui.version

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.model.SimplyData
import ru.mykvm.cryptoneed.data.model.retrofit.VersionRetrofitImpl
import ru.mykvm.cryptoneed.databinding.VersionFragmentBinding
import ru.mykvm.cryptoneed.ui.cabinet.Cabinet
import ru.mykvm.cryptoneed.ui.cabinet.CabinetFragment
import ru.mykvm.cryptoneed.viewmodels.VersionViewModel
import android.content.Intent
import android.net.Uri
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomappbar.BottomAppBar
import kotlinx.android.synthetic.main.main_fragment.*
import ru.mykvm.cryptoneed.ui.faq.FaqFragment
import ru.mykvm.cryptoneed.ui.historyselector.HistorySelectorFragment
import ru.mykvm.cryptoneed.ui.menu.BottomNavigationDrawerFragment
import ru.mykvm.cryptoneed.ui.payment.PaymentFragment
import ru.mykvm.cryptoneed.ui.support.SupportFragment

class VersionFragment : Fragment() {
    private var _binding: VersionFragmentBinding? = null
    private val binding get() = _binding!!
    lateinit var serverVersion:String
    private val versionRetrofitImpl: VersionRetrofitImpl = VersionRetrofitImpl()
    companion object {
        fun newInstance() = VersionFragment()
        private var isMain = true
    }
    private lateinit var viewModel: VersionViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = VersionFragmentBinding.inflate(inflater, container, false)
        val update = binding.update
        update.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://cryptoneed.ru/download/cryptoneed.apk")))
            activity?.finish()
        }
        sendServerRequest()
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(VersionViewModel::class.java)
        // TODO: Use the ViewModel
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    private fun sendServerRequest() {
        //getUserID from Cabinet
        val cabinet: Cabinet? = activity as Cabinet?
        val userID = cabinet?.getUserID().toString()
        versionRetrofitImpl.getRequest().getVersion().enqueue(object :
            Callback<SimplyData> {
            override fun onResponse(
                call: Call<SimplyData>,
                response: Response<SimplyData>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    renderData(response.body(), null)
                } else {
                    renderData(null, Throwable(resources.getString(R.string.server_response_empty)))
                }
            }
            override fun onFailure(call: Call<SimplyData>, t: Throwable) {
                renderData(null, t)
            }
        })
    }
    private fun renderData(dataModel: SimplyData?, error: Throwable?) {
        if (dataModel == null || error != null) {
            //Error
            Toast.makeText(context, error?.message, Toast.LENGTH_SHORT).show()
        } else {
            val response = dataModel.response
            if (response.isNullOrEmpty()) {
                //"Response is empty"
                Toast.makeText(context, getString(R.string.version_server_error), Toast.LENGTH_SHORT).show()
            } else {
                //get response
                if(response.contains("error0"))
                {
                    Toast.makeText(context, getString(R.string.version_server_error), Toast.LENGTH_SHORT).show()
                }
                else{
                    serverVersion = response
                    val currentVersion = binding.currentVersion.text
                    val update = binding.update
                    val warning = binding.warning
                 if(currentVersion.equals(serverVersion))
                 {
                     warning.text = resources.getString(R.string.version_ok)
                     update.isEnabled = false
                 }
                    else{
                     warning.text = resources.getString(R.string.version_error)
                     update.isEnabled = true
                 }
                }
            }
        }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //pair list from menu
        setBottomAppBar(view)
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_bottom_bar, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.faq -> goToFaq()
            R.id.history -> checkHistory()
            R.id.menu_support -> goToSupport()
            R.id.menu_payment -> goToPayment()
            R.id.menu_update -> toast(resources.getString(R.string.you_are_here))
            R.id.menu_web -> goToWeb()
            android.R.id.home -> {
                activity?.let {
                    BottomNavigationDrawerFragment().show(it.supportFragmentManager, "tag")
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
    private fun setBottomAppBar(view: View) {
        val context = activity as Cabinet
        context.setSupportActionBar(view.findViewById(R.id.bottom_app_bar))
        bottom_app_bar.navigationIcon =
            ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
        setHasOptionsMenu(true)
        fab.setOnClickListener {
            if (VersionFragment.isMain) {
                VersionFragment.isMain = false
                bottom_app_bar.navigationIcon = null
                // bottom_app_bar.navigationIcon = ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
                fab.setImageDrawable(
                    ContextCompat.getDrawable(context,
                        R.drawable.ic_baseline_home_24
                    ))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar_other_screen)
            } else {
                VersionFragment.isMain = true
                bottom_app_bar.navigationIcon =
                    ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
                fab.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cn))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar)
                //go to Pair List
                val cabinetFragment = CabinetFragment()
                val transaction = activity?.supportFragmentManager?.beginTransaction()
                transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                transaction?.replace(R.id.container, cabinetFragment)
                transaction?.commit()
            }
        }
    }
    private fun checkHistory()
    {
        val historySelectorFragment = HistorySelectorFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, historySelectorFragment)
        transaction?.commit()
    }
    private fun goToSupport()
    {
        val supportFragment = SupportFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, supportFragment)
        transaction?.commit()
    }
    private fun goToFaq()
    {
        val faqFragment = FaqFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, faqFragment)
        transaction?.commit()
    }
    private fun goToPayment()
    {
        val paymentFragment = PaymentFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, paymentFragment)
        transaction?.commit()
    }
    private fun goToWeb()
    {
        val uris = Uri.parse("https://cryptoneed.ru/")
        val intents = Intent(Intent.ACTION_VIEW, uris)
        val b = Bundle()
        b.putBoolean("new_window", true)
        intents.putExtras(b)
        activity?.startActivity(intents)
    }
    private fun Fragment.toast(string: String?) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).apply {
            setGravity(Gravity.BOTTOM, 0, 250)
            show()
        }
    }
}