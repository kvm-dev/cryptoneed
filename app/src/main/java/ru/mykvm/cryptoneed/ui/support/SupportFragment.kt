package ru.mykvm.cryptoneed.ui.support

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomappbar.BottomAppBar
import kotlinx.android.synthetic.main.main_fragment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.model.SimplyData
import ru.mykvm.cryptoneed.databinding.SupportFragmentBinding
import ru.mykvm.cryptoneed.viewmodels.SupportViewModel
import ru.mykvm.cryptoneed.data.model.retrofit.SupportRetrofitImpl
import ru.mykvm.cryptoneed.ui.cabinet.Cabinet
import ru.mykvm.cryptoneed.ui.cabinet.CabinetFragment
import ru.mykvm.cryptoneed.ui.faq.FaqFragment
import ru.mykvm.cryptoneed.ui.historyselector.HistorySelectorFragment
import ru.mykvm.cryptoneed.ui.login.afterTextChanged
import ru.mykvm.cryptoneed.ui.menu.BottomNavigationDrawerFragment
import ru.mykvm.cryptoneed.ui.payment.PaymentFragment
import ru.mykvm.cryptoneed.ui.version.VersionFragment

class SupportFragment : Fragment() {
    private var _binding: SupportFragmentBinding? = null
    private val binding get() = _binding!!
    private val supportRetrofitImpl: SupportRetrofitImpl = SupportRetrofitImpl()
    private var subjectMessage="application"
    private var messageText = ""
    companion object {
        fun newInstance() = SupportFragment()
        private var isMain = true
    }
    private lateinit var viewModel: SupportViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SupportFragmentBinding.inflate(inflater, container, false)
        val subjectApplication = binding.application
        val subjectPayment = binding.payment
        val subjectOffer = binding.offers
        val message = binding.message
        val sendButton = binding.sendToSupport
        val toFaqButton = binding.goToFaq
        message.afterTextChanged {
            if(message.text.isNullOrEmpty())
            {
                Toast.makeText(context, getString(R.string.support_error), Toast.LENGTH_SHORT).show()
                sendButton.isEnabled = false
            }
            else
            {
                messageText = message.text.toString()
                sendButton.isEnabled = true
            }
        }
        subjectApplication.setOnClickListener{
            subjectMessage = "application"
        }
        subjectPayment.setOnClickListener{
            subjectMessage = "payment"
        }
        subjectOffer.setOnClickListener{
            subjectMessage = "offer"
        }
        sendButton.setOnClickListener {
            if(messageText.isNullOrEmpty())
            {
                Toast.makeText(context, getString(R.string.support_error), Toast.LENGTH_SHORT).show()
                sendButton.isEnabled = false
            }
            else{
                sendButton.isEnabled = true
                sendServerRequest()
            }
        }
        toFaqButton.setOnClickListener {
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            val faqFragment = FaqFragment()
            transaction?.replace(R.id.container, faqFragment)
            transaction?.commit()
        }
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SupportViewModel::class.java)
        // TODO: Use the ViewModel
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //pair list from menu
        setBottomAppBar(view)
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    private fun sendServerRequest() {
        //getUserID from Cabinet
        val cabinet: Cabinet? = activity as Cabinet?
        val userID = cabinet?.getUserID().toString()
        supportRetrofitImpl.getRequest().sendToSupport(userID, subjectMessage, messageText).enqueue(object :
            Callback<SimplyData> {
            override fun onResponse(
                call: Call<SimplyData>,
                response: Response<SimplyData>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    renderData(response.body(), null)
                } else {
                    renderData(null, Throwable(resources.getString(R.string.server_response_empty)))
                }
            }
            override fun onFailure(call: Call<SimplyData>, t: Throwable) {
                renderData(null, t)
            }
        })
    }
    private fun renderData(dataModel: SimplyData?, error: Throwable?) {
        if (dataModel == null || error != null) {
            //Error
            Toast.makeText(context, error?.message, Toast.LENGTH_SHORT).show()
        } else {
            val response = dataModel.response
            if (response.isNullOrEmpty()) {
                //"Response is empty"
                Toast.makeText(context, getString(R.string.server_response_empty), Toast.LENGTH_SHORT).show()
            } else {
                //get response
                if(response.contains("error"))
                {
                    Toast.makeText(context, getString(R.string.support_error), Toast.LENGTH_SHORT).show()
                }
                else{
                    Toast.makeText(context, getString(R.string.support_ok), Toast.LENGTH_SHORT).show()
                    val transaction = activity?.supportFragmentManager?.beginTransaction()
                    transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    val cabinetFragment = CabinetFragment()
                    transaction?.replace(R.id.container, cabinetFragment)
                    transaction?.commit()
                }
            }
        }
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_bottom_bar, menu)

    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.faq -> goToFaq()
            R.id.history -> checkHistory()
            R.id.menu_support -> toast(resources.getString(R.string.you_are_here))
            R.id.menu_payment -> goToPayment()
            R.id.menu_update -> checkVersion()
            R.id.menu_web -> goToWeb()
            android.R.id.home -> {
                activity?.let {
                    BottomNavigationDrawerFragment().show(it.supportFragmentManager, "tag")
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
    private fun setBottomAppBar(view: View) {
        val context = activity as Cabinet
        context.setSupportActionBar(view.findViewById(R.id.bottom_app_bar))
        bottom_app_bar.navigationIcon =
            ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
        setHasOptionsMenu(true)
        fab.setOnClickListener {
            if (SupportFragment.isMain) {
                SupportFragment.isMain = false
                bottom_app_bar.navigationIcon = null
                // bottom_app_bar.navigationIcon = ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
                fab.setImageDrawable(
                    ContextCompat.getDrawable(context,
                        R.drawable.ic_baseline_home_24
                    ))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar_other_screen)
            } else {
                SupportFragment.isMain = true
                bottom_app_bar.navigationIcon =
                    ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
                fab.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cn))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar)
                //go to Pair List
                val cabinetFragment = CabinetFragment()
                val transaction = activity?.supportFragmentManager?.beginTransaction()
                transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                transaction?.replace(R.id.container, cabinetFragment)
                transaction?.commit()
            }
        }
    }
    private fun checkHistory()
    {
        val historySelectorFragment = HistorySelectorFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, historySelectorFragment)
        transaction?.commit()
    }
    private fun goToFaq()
    {
        val faqFragment = FaqFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, faqFragment)
        transaction?.commit()
    }
    private fun goToPayment()
    {
        val paymentFragment = PaymentFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, paymentFragment)
        transaction?.commit()
    }
    private fun checkVersion()
    {
        val versionFragment = VersionFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, versionFragment)
        transaction?.commit()
    }
    private fun goToWeb()
    {
        val uris = Uri.parse("https://cryptoneed.ru/")
        val intents = Intent(Intent.ACTION_VIEW, uris)
        val b = Bundle()
        b.putBoolean("new_window", true)
        intents.putExtras(b)
        activity?.startActivity(intents)
    }
    private fun Fragment.toast(string: String?) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).apply {
            setGravity(Gravity.BOTTOM, 0, 250)
            show()
        }
    }
}