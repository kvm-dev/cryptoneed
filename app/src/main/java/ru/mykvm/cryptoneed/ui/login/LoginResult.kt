package ru.mykvm.cryptoneed.ui.login

import ru.mykvm.cryptoneed.data.model.SimplyData

data class LoginResult(
    val success: SimplyData? = null,
    val error: Int? = null
)