package ru.mykvm.cryptoneed.ui.login

data class RegFormState(
    val usernameError: Int? = null,
    val userPasswordError: Int? = null,
    val apiKeyError: Int? = null,
    val secretKeyError: Int? = null,
    val isDataValid: Boolean = false
)
