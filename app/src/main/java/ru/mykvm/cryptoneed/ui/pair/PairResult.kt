package ru.mykvm.cryptoneed.ui.pair
import ru.mykvm.cryptoneed.data.model.SimplyData

data class PairResult(
    val success: SimplyData? = null,
    val error: Int? = null
)