package ru.mykvm.cryptoneed.ui.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_reg.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.model.SimplyData
import ru.mykvm.cryptoneed.data.model.retrofit.ResponseRegRetrofitImp
import ru.mykvm.cryptoneed.databinding.FragmentRegBinding
import ru.mykvm.cryptoneed.viewmodels.RegViewModel
import ru.mykvm.cryptoneed.viewmodels.RegViewModelFactory

class FragmentReg : Fragment(R.layout.fragment_reg){
    private lateinit var regViewModel: RegViewModel
    private var fragmentRegBinding: FragmentRegBinding? = null
    //response registration
    private var responseregistration:String = "" //  //don't know but for more devices we gain null exception error
    private val retrofitImpl: ResponseRegRetrofitImp = ResponseRegRetrofitImp()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentRegBinding.bind(view)
        fragmentRegBinding = binding
        //тут обращаемся к элементам
        val username = binding.username
        val userpassword = binding.userpassword
        val login = binding.login
        val registration = binding.registration
        val apikey = binding.apikey
        val secretkey = binding.secretkey
        val promo = binding.promo
        val whereget = binding.whereget
        val rulesAndPolicy = binding.rulesAndPolicy
        //checkerrors
        regViewModel = ViewModelProvider(this, RegViewModelFactory())
            .get(RegViewModel::class.java)
        regViewModel.regFormState.observe(viewLifecycleOwner, Observer {
            val regState = it ?: return@Observer
            // disable reg button unless both username / password / name/ phone / channel is valid
            registration.isEnabled = regState.isDataValid
            if (regState.usernameError != null) {
                username.error = getString(regState.usernameError)
            }
            if (regState.userPasswordError != null) {
                userpassword.error = getString(regState.userPasswordError)
            }
            if (regState.apiKeyError != null) {
                apikey.error = getString(regState.apiKeyError)
            }
            if (regState.secretKeyError != null) {
                secretkey.error = getString(regState.secretKeyError)
            }
        })
        regViewModel.regResult.observe(viewLifecycleOwner, Observer {
            val regResult = it ?: return@Observer

            if (regResult.error != null) {
                showRegFailed(regResult.error)
            }
            if (regResult.success != null) {
                updateUiWithUser(regResult.success)
            }
            activity?.setResult(Activity.RESULT_OK)
        })
        username.afterTextChanged {
            regViewModel.regDataChanged(
                username.text.toString(),
                userpassword.text.toString(),
                apikey.text.toString(),
                secretkey.text.toString()
            )
        }
        userpassword.afterTextChanged {
            regViewModel.regDataChanged(
                username.text.toString(),
                userpassword.text.toString(),
                apikey.text.toString(),
                secretkey.text.toString()
            )
        }
        apikey.afterTextChanged {
            regViewModel.regDataChanged(
                username.text.toString(),
                userpassword.text.toString(),
                apikey.text.toString(),
                secretkey.text.toString()
            )
        }
        secretkey.apply {
            afterTextChanged {
                regViewModel.regDataChanged(
                    username.text.toString(),
                    userpassword.text.toString(),
                    apikey.text.toString(),
                    secretkey.text.toString()
                )
            }
            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        regViewModel.reg(
                            username.text.toString(),
                            userpassword.text.toString(),
                            apikey.toString(),
                            secretkey.text.toString(),
                            promo.text.toString()
                        )
                }
                false
            }
            registration.setOnClickListener {
                sendServerRequest(username.text.toString(), userpassword.text.toString(), apikey.text.toString(), secretkey.text.toString(), promo.text.toString())
            }
            login.setOnClickListener {
                val fragmentLogin = LoginFragment()
                activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.FragmentContainer, fragmentLogin, "log")
                    ?.commit();
            }
        }
        fun openNewTabWindow(urls: String, context: Context) {
            val uris = Uri.parse(urls)
            val intents = Intent(Intent.ACTION_VIEW, uris)
            val b = Bundle()
            b.putBoolean("new_window", true)
            intents.putExtras(b)
            context.startActivity(intents)
        }
        whereget.setOnClickListener {
            activity?.let { it1 -> openNewTabWindow("https://www.binance.com/ru/support/faq/360002502072", it1) }
        }
        rulesAndPolicy.setOnClickListener {
            activity?.let { it1 -> openNewTabWindow("https://cryptoneed.ru/rules-and-policy/", it1) }
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        fragmentRegBinding = null
    }
    private fun updateUiWithUser(model: SimplyData) {
        val congratulate = getString(R.string.congratulate)
        if(responseregistration.isNullOrEmpty() || responseregistration.contains("error"))
        {
            if(responseregistration.contains("error0"))
            {
                Toast.makeText(activity, getString(R.string.registration_error0), Toast.LENGTH_SHORT).show()
            }
            else if(responseregistration.contains("error1"))
            {
                Toast.makeText(activity, getString(R.string.registration_error1), Toast.LENGTH_SHORT).show()
            }
        }
        else
        {
            Toast.makeText(
                activity,
                "$congratulate",
                Toast.LENGTH_LONG
            ).show()
            val fragmentLogin = LoginFragment()
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.FragmentContainer, fragmentLogin, "log")
                ?.commit();
        }
    }
    private fun showRegFailed(@StringRes errorString: Int) {
        Toast.makeText(activity, errorString, Toast.LENGTH_SHORT).show()
    }
    //methods check reg
    private fun sendServerRequest(
        username: String,
        userpassword: String,
        apikey: String,
        secretkey: String,
        promo: String
    ) {
        retrofitImpl.getRequest().getResponseReg(username, userpassword, apikey, secretkey, promo).enqueue(object :
            Callback<SimplyData> {

            override fun onResponse(
                call: Call<SimplyData>,
                response: Response<SimplyData>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    renderData(response.body(), null)
                } else {
                    renderData(null, Throwable(getString(R.string.server_response_empty)))
                }
            }
            override fun onFailure(call: Call<SimplyData>, t: Throwable) {
                renderData(null, t)
            }
        })
    }
    private fun renderData(dataModel: SimplyData?, error: Throwable?) {
        if (dataModel == null || error != null) {
            //Error
            Toast.makeText(activity, error?.message, Toast.LENGTH_SHORT).show()
        } else {
            val response = dataModel.response
            if (response.isNullOrEmpty()) {
                //"Response is empty"
                Toast.makeText(activity, getString(R.string.server_response_empty), Toast.LENGTH_SHORT).show()
            } else {
                //get response
                responseregistration = response
                regViewModel.reg(username.text.toString(), userpassword.text.toString(), apikey.text.toString(), secretkey.text.toString(),promo.text.toString())
            }
        }
    }
}

