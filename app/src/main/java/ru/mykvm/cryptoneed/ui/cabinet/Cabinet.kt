package ru.mykvm.cryptoneed.ui.cabinet

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.model.SimplyData
import ru.mykvm.cryptoneed.data.model.retrofit.IdRetrofitImpl
import ru.mykvm.cryptoneed.data.model.retrofit.SendPairSettingsImpl

class Cabinet: AppCompatActivity() {
    val EXTRA_UID = "User_ID"
    private lateinit var userID:String
    private lateinit var responsePairSettings:String
    private val retrofitImpl: IdRetrofitImpl = IdRetrofitImpl()
    private val sendPairSettingsImpl: SendPairSettingsImpl = SendPairSettingsImpl()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cabinet)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CabinetFragment.newInstance())
                .commitNow()
        }
    }
    //to fragment get UserID funtion
    fun getUserID(): String? {
        userID = intent.getStringExtra(EXTRA_UID).toString()
        return userID
    }
     fun sendServerRequest(pair: String, busdSum: Int, priceBuy: Double, priceSell: Double, deltaBuy: Int, deltaSell: Int, userID: String, tradeMode: Int, pairStatus: Int) {
        sendPairSettingsImpl.getRequest().updateSettings(pair,busdSum, priceBuy, priceSell, deltaBuy, deltaSell, userID, tradeMode, pairStatus).enqueue(object :
            Callback<SimplyData> {

            override fun onResponse(
                call: Call<SimplyData>,
                response: Response<SimplyData>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    renderData(response.body(), null)
                } else {
                    renderData(null, Throwable(resources.getString(R.string.server_response_empty)))
                }
            }
            override fun onFailure(call: Call<SimplyData>, t: Throwable) {
                renderData(null, t)
            }
        })
    }
    private fun renderData(dataModel: SimplyData?, error: Throwable?) {
        if (dataModel == null || error != null) {
            //Error
            Toast.makeText(this, error?.message, Toast.LENGTH_SHORT).show()
        } else {
            val response = dataModel.response
            if (response.isNullOrEmpty()) {
                //"Response is empty"
                Toast.makeText(this, getString(R.string.server_response_empty), Toast.LENGTH_SHORT).show()
            } else {
                //get response
                responsePairSettings = response
                if(responsePairSettings.contains("error0"))
                {
                    Toast.makeText(this, getString(R.string.update_pair_settings_error0), Toast.LENGTH_SHORT).show()
                }
                else if(responsePairSettings.contains("error1")){
                    Toast.makeText(this, getString(R.string.update_pair_settings_error1), Toast.LENGTH_SHORT).show()
                }
                else{
                    Toast.makeText(this, getString(R.string.bot_config_ok), Toast.LENGTH_SHORT).show()
                    val transaction = this.supportFragmentManager?.beginTransaction()
                    val cabinetFragment = CabinetFragment()
                    transaction?.replace(R.id.container, cabinetFragment)
                    transaction?.commit()
                }
            }
        }
    }
    //override hard backbutton
    override fun onBackPressed() {
        AlertDialog.Builder(this).apply {
            setTitle(getString(R.string.exit_confirm))
            setMessage(getString(R.string.exit_question))

            setPositiveButton(getString(R.string.exit_yes)) { _, _ ->
                super.onBackPressed()
            }

            setNegativeButton(getString(R.string.exit_no)) { _, _ ->
                // if user press no, then return the activity
                Toast.makeText(
                    this@Cabinet, getString(R.string.exit_huh),
                    Toast.LENGTH_LONG
                ).show()
            }
            setCancelable(true)
        }.create().show()
    }
}
