package ru.mykvm.cryptoneed.ui.cabinet

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomappbar.BottomAppBar
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.main_fragment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.adapters.PairRecyclerAdapter
import ru.mykvm.cryptoneed.data.model.PairServices
import ru.mykvm.cryptoneed.data.model.common.CommonPair
import ru.mykvm.cryptoneed.data.model.retrofit.interfaces.RetrofitPair
import ru.mykvm.cryptoneed.databinding.MainFragmentBinding
import ru.mykvm.cryptoneed.ui.faq.FaqFragment
import ru.mykvm.cryptoneed.ui.support.SupportFragment
import ru.mykvm.cryptoneed.ui.historyselector.HistorySelectorFragment
import ru.mykvm.cryptoneed.ui.menu.BottomNavigationDrawerFragment
import ru.mykvm.cryptoneed.ui.payment.PaymentFragment
import ru.mykvm.cryptoneed.ui.version.VersionFragment

class CabinetFragment : Fragment() {
    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var mPair: RetrofitPair
    private lateinit var layoutManagerPair: LinearLayoutManager
    lateinit var adapterPair: PairRecyclerAdapter
    private lateinit var dialog: AlertDialog
    private lateinit var userIDFromCabinet: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        mPair = CommonPair.retrofitPair
        binding.recyclerViewPair.setHasFixedSize(true)
        layoutManagerPair = LinearLayoutManager(activity)
        binding.recyclerViewPair.layoutManager = layoutManagerPair
        dialog = SpotsDialog.Builder().setCancelable(true).setContext(activity).build()
        //getUserID from Cabinet
        val cabinet: Cabinet? = activity as Cabinet?
        userIDFromCabinet = cabinet?.getUserID().toString()
        //send request to server
        getAllPairList()
        return root
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //pair list from menu
        setBottomAppBar(view)
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_bottom_bar, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.faq -> goToFaq()
            R.id.history -> checkHistory()
            R.id.menu_support -> goToSupport()
            R.id.menu_payment -> goToPayment()
            R.id.menu_update -> checkVersion()
            R.id.menu_web -> goToWeb()
            android.R.id.home -> {
                activity?.let {
                   BottomNavigationDrawerFragment().show(it.supportFragmentManager, "tag")
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
    private fun setBottomAppBar(view: View) {
        val context = activity as Cabinet
        context.setSupportActionBar(view.findViewById(R.id.bottom_app_bar))
        bottom_app_bar.navigationIcon =
            ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
        setHasOptionsMenu(true)
        fab.setOnClickListener {
            if (isMain) {
                isMain = false
                bottom_app_bar.navigationIcon = null
               // bottom_app_bar.navigationIcon = ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
                fab.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_baseline_home_24
                ))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar_other_screen)
            } else {
                isMain = true
                bottom_app_bar.navigationIcon =
                    ContextCompat.getDrawable(context, R.drawable.ic_baseline_sync_alt_24)
                bottom_app_bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
                fab.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cn))
                bottom_app_bar.replaceMenu(R.menu.menu_bottom_bar)
                toast(resources.getString(R.string.you_are_here))
            }
        }
    }
    private fun Fragment.toast(string: String?) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).apply {
            setGravity(Gravity.BOTTOM, 0, 250)
            show()
        }
    }
    companion object {
        fun newInstance() = CabinetFragment()
        private var isMain = true
    }
    private fun getAllPairList() {
        dialog.show()
        mPair.getPairList(userIDFromCabinet).enqueue(object :
            Callback<MutableList<PairServices>> {
            override fun onFailure(call: Call<MutableList<PairServices>>, t: Throwable) {
            }
            override fun onResponse(call: Call<MutableList<PairServices>>, response: Response<MutableList<PairServices>>) {
                adapterPair = activity?.let { PairRecyclerAdapter(userIDFromCabinet,it.baseContext, response.body() as MutableList<PairServices>) }!!
                adapterPair.notifyDataSetChanged()
                recyclerViewPair.adapter = adapterPair
                dialog.dismiss()
            }
        })
    }
    private fun checkHistory()
    {
        val historySelectorFragment = HistorySelectorFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, historySelectorFragment)
        transaction?.commit()
    }
    private fun goToSupport()
    {
        val supportFragment = SupportFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, supportFragment)
        transaction?.commit()
    }
    private fun goToFaq()
    {
        val faqFragment = FaqFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, faqFragment)
        transaction?.commit()
    }
    private fun goToPayment()
    {
        val paymentFragment = PaymentFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, paymentFragment)
        transaction?.commit()
    }
    private fun checkVersion()
    {
        val versionFragment = VersionFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction?.replace(R.id.container, versionFragment)
        transaction?.commit()
    }
    private fun goToWeb()
    {
        val uris = Uri.parse("https://cryptoneed.ru/")
        val intents = Intent(Intent.ACTION_VIEW, uris)
        val b = Bundle()
        b.putBoolean("new_window", true)
        intents.putExtras(b)
        activity?.startActivity(intents)
    }

}
