package ru.mykvm.cryptoneed.ui.login

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.model.SimplyData
import ru.mykvm.cryptoneed.data.model.retrofit.IdRetrofitImpl
import ru.mykvm.cryptoneed.databinding.FragmentLoginBinding
import ru.mykvm.cryptoneed.ui.cabinet.Cabinet
import ru.mykvm.cryptoneed.viewmodels.LoginViewModel

public class LoginFragment : Fragment(R.layout.fragment_login) {
    private lateinit var loginViewModel: LoginViewModel
    private var fragmentLoginBinding: FragmentLoginBinding? = null
    private val EXTRA_UID = "User_ID"
    //userid
    private var userID:String ="" //don't know but for more devices we gain null exception error
    private val retrofitImpl: IdRetrofitImpl = IdRetrofitImpl()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentLoginBinding.bind(view)
        fragmentLoginBinding = binding
        //тут обращаемся к элементам
        val username = binding.username
        val userpassword = binding.userpassword
        val login = binding.login
        val regbutton = binding.registration
        //val loading = binding.loading
        //check errors
        loginViewModel = ViewModelProvider(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)
        loginViewModel.loginFormState.observe(viewLifecycleOwner, Observer {
            val loginState = it ?: return@Observer
            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid
            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.userPasswordError != null) {
                userpassword.error = getString(loginState.userPasswordError)
            }
        })
        loginViewModel.loginResult.observe(viewLifecycleOwner, Observer {
            val loginResult = it ?: return@Observer
            //loading.visibility = View.GONE
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error)
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
            }
            activity?.setResult(Activity.RESULT_OK)
        })
        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                userpassword.text.toString()
            )
        }
        userpassword.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    userpassword.text.toString()
                )
            }
            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            username.text.toString(),
                            userpassword.text.toString()
                        )
                }
                false
            }
            login.setOnClickListener {
                //loading.visibility = View.VISIBLE
                sendServerRequest(username.text.toString(), userpassword.text.toString())
            }
            regbutton.setOnClickListener {
                val fragmentReg = FragmentReg()
                activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.FragmentContainer, fragmentReg, "reg")
                    ?.commit();
            }
        }
    }
    override fun onDestroyView() {
        // Consider not storing the binding instance in a field, if not needed.
        fragmentLoginBinding = null
        super.onDestroyView()
    }
    private fun updateUiWithUser(model: SimplyData) {
        val welcome = getString(R.string.welcome)
        // TODO : initiate successful logged in experience
        if(userID.isNullOrEmpty() || userID.contains("error"))
        {
            if(userID.contains("error0"))
            {
                Toast.makeText(activity, getString(R.string.auth_error0), Toast.LENGTH_SHORT).show()
            }
            else if(userID.contains("error1"))
            {
                Toast.makeText(activity, getString(R.string.auth_error1), Toast.LENGTH_SHORT).show()
            }
            else if(userID.contains("error2"))
            {
                userpassword.error = getString(R.string.auth_error2)
            }
            else if(userID.contains("error3"))
            {
                Toast.makeText(activity, getString(R.string.auth_error3), Toast.LENGTH_SHORT).show()
            }
            else if(userID.contains("error4"))
            {
                Toast.makeText(activity, getString(R.string.auth_error4), Toast.LENGTH_SHORT).show()
            }
        }
        else
        {
            Toast.makeText(
                activity,
                // "$welcome $userID",
                "$welcome",
                Toast.LENGTH_LONG
            ).show()
            val intent = Intent(activity, Cabinet::class.java).apply {
                putExtra(EXTRA_UID,  userID)
            }
            startActivity(intent)
            //Complete and destroy login activity once successful
            activity?.finish()
        }
    }
    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(activity, errorString, Toast.LENGTH_SHORT).show()
    }
    //methods check auth
    private fun sendServerRequest(login:String, password:String) {
        retrofitImpl.getRequest().getUserID(login, password).enqueue(object :
            Callback<SimplyData> {

            override fun onResponse(
                call: Call<SimplyData>,
                response: Response<SimplyData>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    renderData(response.body(), null)
                } else {
                    renderData(null, Throwable(getString(R.string.server_response_empty)))
                }
            }
            override fun onFailure(call: Call<SimplyData>, t: Throwable) {
                renderData(null, t)
            }
        })
    }
    private fun renderData(dataModel: SimplyData?, error: Throwable?) {
        if (dataModel == null || error != null) {
            //Error
            Toast.makeText(activity, error?.message, Toast.LENGTH_SHORT).show()
        } else {
            val response = dataModel.response
            if (response.isNullOrEmpty()) {
                //"Response is empty"
                Toast.makeText(activity, getString(R.string.server_response_empty), Toast.LENGTH_SHORT).show()
            } else {
                //get userID
                userID = response
                loginViewModel.login(username.text.toString(), userpassword.text.toString())
            }
        }
    }
}
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

