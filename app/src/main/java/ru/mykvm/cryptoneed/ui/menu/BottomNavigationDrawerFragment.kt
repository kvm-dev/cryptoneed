package ru.mykvm.cryptoneed.ui.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_navigation_layout.*
import kotlinx.android.synthetic.main.pair_item.view.*
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.PairDetails
import ru.mykvm.cryptoneed.ui.cabinet.Cabinet
import ru.mykvm.cryptoneed.ui.pair.PairSettingsFragment


class BottomNavigationDrawerFragment : BottomSheetDialogFragment() {
    val pairData = "pairData"
    private lateinit var userID:String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val cabinet: Cabinet? = activity as Cabinet?
        userID = cabinet?.getUserID().toString()
        return inflater.inflate(R.layout.bottom_navigation_layout, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fun pairDataTransaction(pair: String)
        {
            var details = PairDetails(userID!!, pair)
            val settingsFragment = PairSettingsFragment()
            val bundle = Bundle()
            bundle.putParcelable(pairData, details)
            settingsFragment.arguments = bundle
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction?.replace(R.id.container, settingsFragment)
            transaction?.commit()
        }

        navigation_view.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.ada -> pairDataTransaction("ADA")
                R.id.alice -> pairDataTransaction("ALICE")
                R.id.atom -> pairDataTransaction("ATOM")
                R.id.bnb -> pairDataTransaction("BNB")
                R.id.btc -> pairDataTransaction("BTC")
                R.id.celr -> pairDataTransaction("CELR")
                R.id.dodo -> pairDataTransaction("DODO")
                R.id.doge -> pairDataTransaction("DOGE")
                R.id.dot -> pairDataTransaction("DOT")
                R.id.eth -> pairDataTransaction("ETH")
                R.id.for_ -> pairDataTransaction("FOR")
                R.id.gala -> pairDataTransaction("GALA")
                R.id.pond -> pairDataTransaction("POND")
                R.id.sol -> pairDataTransaction("SOL")
                R.id.win -> pairDataTransaction("WIN")
            }
            true
        }
    }
}
