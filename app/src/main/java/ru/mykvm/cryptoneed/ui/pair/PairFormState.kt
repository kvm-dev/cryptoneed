package ru.mykvm.cryptoneed.ui.pair

data class PairFormState(
    val tradeQuantityError: Int? = null,
    val manualBuyPriceError: Int? = null,
    val manualSellPriceError: Int? = null,
    val downDeltaError: Int? = null,
    val autoPercentBenefitError: Int? = null,
    val isDataValid: Boolean = false
)