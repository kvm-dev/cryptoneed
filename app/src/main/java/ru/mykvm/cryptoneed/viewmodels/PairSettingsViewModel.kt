package ru.mykvm.cryptoneed.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.PairRepository
import ru.mykvm.cryptoneed.data.Result
import ru.mykvm.cryptoneed.data.model.SimplyData
import ru.mykvm.cryptoneed.ui.pair.PairFormState
import ru.mykvm.cryptoneed.ui.pair.PairResult

class PairSettingsViewModel(private val pairRepository: PairRepository) : ViewModel() {
    private val _pairForm = MutableLiveData<PairFormState>()
    val pairFormState: LiveData<PairFormState> = _pairForm
    private val _pairResult = MutableLiveData<PairResult>()
    val pairResult: LiveData<PairResult> = _pairResult
    fun update(tradeQuantity: Int, manualBuyPrice: Double, manualSellPrice: Double, downDelta: Int, percentBenefit: Int) {
        if (tradeQuantity>=50 && manualBuyPrice>0 && manualSellPrice>0 && downDelta>=1 && percentBenefit>=1) {
            // can be launched in a separate asynchronous job
            val result = pairRepository.update(tradeQuantity, manualBuyPrice, manualSellPrice, downDelta, percentBenefit)
            if (result is Result.Success) {
                _pairResult.value =
                    PairResult(success = SimplyData(response = result.data.response))
            } else {
                _pairResult.value = PairResult(error = R.string.pair_settings_server_error)
            }
        }
        else{
            _pairForm.value = PairFormState(tradeQuantityError = R.string.pair_settings_server_error)
        }
    }
    fun pairDataChanged(tradeQuantity:Int, manualBuyPrice:Double, manualSellPrice:Double, downDelta: Int, percentBenefit: Int) {
        if (!isTradeQuantityValid(tradeQuantity)) {
            _pairForm.value = PairFormState(tradeQuantityError = R.string.error_quantity)
        } else if (!isManualBuyPriceValid(manualBuyPrice)) {
            _pairForm.value = PairFormState(manualBuyPriceError = R.string.error_manual_buy_price)
        } else if (!isManualSellPriceValid(manualSellPrice)) {
            _pairForm.value = PairFormState(manualSellPriceError = R.string.error_manual_sell_price)
        }else if (!isDownDeltaValid(downDelta)) {
            _pairForm.value = PairFormState(downDeltaError = R.string.error_down_delta)
        }else if (!isPercentBenefitValid(percentBenefit)) {
            _pairForm.value = PairFormState(autoPercentBenefitError = R.string.error_percent_benefit)
        }
        else {
            _pairForm.value = PairFormState(isDataValid = true)
        }
    }
    // Validation of Quantity
    private fun isTradeQuantityValid(tradeQuantity: Int): Boolean {
        return tradeQuantity>= 50
    }
    // Validation of Manual Buy Price
    private fun isManualBuyPriceValid(manualBuyPrice: Double): Boolean {
        return manualBuyPrice>0
    }
    // Validation of Manual Sell Price
    private fun isManualSellPriceValid(manualSellPrice: Double): Boolean {
        return manualSellPrice>0
    }
    // Validation of Down Delta
    private fun isDownDeltaValid(downDelta: Int): Boolean {
        return downDelta>=1
    }
    // Validation of Percent Benefit
    private fun isPercentBenefitValid(percentBenefit: Int): Boolean {
        return percentBenefit>=1
    }
}