package ru.mykvm.cryptoneed.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.mykvm.cryptoneed.data.RegRepository
import ru.mykvm.cryptoneed.data.RegistrationDataSource

class RegViewModelFactory : ViewModelProvider.Factory{
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RegViewModel::class.java)) {
            return RegViewModel(
                regRepository = RegRepository(
                    dataSource = RegistrationDataSource()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}