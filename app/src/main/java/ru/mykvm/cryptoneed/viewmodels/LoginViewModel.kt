package ru.mykvm.cryptoneed.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.LoginRepository
import ru.mykvm.cryptoneed.data.Result
import ru.mykvm.cryptoneed.data.model.SimplyData
import ru.mykvm.cryptoneed.ui.login.LoginFormState
import ru.mykvm.cryptoneed.ui.login.LoginResult

class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {
    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm
    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult
    fun login(username: String, userpassword: String) {
        if (username.length>=5 && userpassword.length>5 && username.contains('@')) {
            // can be launched in a separate asynchronous job
            val result = loginRepository.login(username, userpassword)
            if (result is Result.Success) {
                _loginResult.value =
                    LoginResult(success = SimplyData(response = result.data.response))
            } else {
                _loginResult.value = LoginResult(error = R.string.login_failed)
            }
        }
        else{
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_password_or_username)
        }
    }
    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(userPasswordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }
    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }
    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}