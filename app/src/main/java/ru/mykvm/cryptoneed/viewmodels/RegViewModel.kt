package ru.mykvm.cryptoneed.viewmodels

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.RegRepository
import ru.mykvm.cryptoneed.data.model.SimplyData
import ru.mykvm.cryptoneed.data.Result
import ru.mykvm.cryptoneed.ui.login.LoginResult
import ru.mykvm.cryptoneed.ui.login.RegFormState

class RegViewModel(private val regRepository: RegRepository) : ViewModel() {

    private val _regForm = MutableLiveData<RegFormState>()
    val regFormState: LiveData<RegFormState> = _regForm
    private val _regResult = MutableLiveData<LoginResult>()
    val regResult: LiveData<LoginResult> = _regResult
    fun reg(username: String, userPassword: String, apiKey: String, secretKey: String, promo: String) {
        if (username.length>=5 &&
            userPassword.length>5 &&
            username.contains('@') &&
            apiKey.length==64 &&
            secretKey.length==64
        ) {
            // can be launched in a separate asynchronous job
            val result = regRepository.reg(username, userPassword, apiKey, secretKey, promo)
            if (result is Result.Success) {
                _regResult.value =
                    LoginResult(success = SimplyData(response = result.data.response))
            } else {
                _regResult.value = LoginResult(error = R.string.reg_failed)
            }
        }
    }
    fun regDataChanged(username: String, userpassword: String, apikey: String, secretkey: String) {
        if (!isApiKeyValid(apikey)) {
            _regForm.value = RegFormState(apiKeyError = R.string.reg_error3)
        }
        else if (!isSecretKeyValid(secretkey)) {
            _regForm.value = RegFormState(secretKeyError = R.string.reg_error4)
        }
        else if (!isUserNameValid(username)) {
            _regForm.value = RegFormState(usernameError = R.string.reg_error1)
        }
        else if (!isUserPasswordValid(userpassword)) {
            _regForm.value = RegFormState(userPasswordError = R.string.reg_error2)
        }
        else {
            _regForm.value = RegFormState(isDataValid = true)
        }
    }
    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }
    // A placeholder password validation check
    private fun isUserPasswordValid(userPassword: String): Boolean {
        return userPassword.length > 5
    }
    // A placeholder name validation check
    private fun isApiKeyValid(apiKey: String): Boolean {
        return apiKey.length == 64
    }
    // A placeholder phone validation check
    private fun isSecretKeyValid(secretKey: String): Boolean {
        return secretKey.length ==64
    }
}