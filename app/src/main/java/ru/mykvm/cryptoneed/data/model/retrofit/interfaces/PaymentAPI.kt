package ru.mykvm.cryptoneed.data.model.retrofit.interfaces

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ru.mykvm.cryptoneed.data.model.SimplyData

interface PaymentAPI {
    @GET("cryptoneed/payment.php")
    fun goToPay(@Query("userID") userID: String): Call<SimplyData>
}