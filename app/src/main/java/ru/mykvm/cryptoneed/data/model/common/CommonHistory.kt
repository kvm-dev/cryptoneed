package ru.mykvm.cryptoneed.data.model.common

import ru.mykvm.cryptoneed.data.model.retrofit.RetrofitClientHistory
import ru.mykvm.cryptoneed.data.model.retrofit.interfaces.RetrofitHistory

object CommonHistory {
    private const val BASE_URL = "https://mykvm.ru/"
    val retrofitHistory: RetrofitHistory
        get() = RetrofitClientHistory.getClientHistory(BASE_URL).create(RetrofitHistory::class.java)
}