package ru.mykvm.cryptoneed.data.model.retrofit.interfaces

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ru.mykvm.cryptoneed.data.model.SimplyData

interface RegAPI {
    @GET("cryptoneed/reg.php")
    fun getResponseReg(@Query("useremail") login: String, @Query("userpassword") password: String, @Query("apikey") apikey: String, @Query("secretkey") secretkey: String, @Query("promo") promo: String): Call<SimplyData>
}