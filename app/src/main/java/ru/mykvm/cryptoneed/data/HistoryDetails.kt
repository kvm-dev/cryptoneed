package ru.mykvm.cryptoneed.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class HistoryDetails(val userID: String, val pair: String) : Parcelable