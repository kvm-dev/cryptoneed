package ru.mykvm.cryptoneed.data.model

class HistoryServices (
    var symbol: String? = null,
    var time: String? = null,
    var isBuyer: String? = null,
    var price: String? = null,
    var qty: String? = null,
    var commission: String? = null,
    var quoteQty: String? = null
)