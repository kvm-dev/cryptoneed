package ru.mykvm.cryptoneed.data.model.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClientPair {
    private var retrofitPair: Retrofit? = null
    fun getClientPair(baseUrl: String): Retrofit {
        if (retrofitPair == null) {
            retrofitPair = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofitPair!!
    }
}
