package ru.mykvm.cryptoneed.data

import ru.mykvm.cryptoneed.data.model.SimplyData

class LoginRepository(val dataSource: LoginDataSource) {
    // in-memory cache of the loggedInUser object
    var user: SimplyData? = null
        private set
    val isLoggedIn: Boolean
        get() = user != null
    init {
        user = null
    }
    fun logout() {
        user = null
        dataSource.logout()
    }
    fun login(username: String, userpassword: String): Result<SimplyData> {
        // handle login
        val result = dataSource.login(username, userpassword)

        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }
        return result
    }
    private fun setLoggedInUser(loggedInUser: SimplyData) {
        this.user = loggedInUser
    }
}