package ru.mykvm.cryptoneed.data.model.retrofit.interfaces

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ru.mykvm.cryptoneed.data.model.InfoPairData

interface PairSettingsAPI {
    @GET("cryptoneed/infopair.php")
    fun getPairSettingsData(@Query("userid") userid: String, @Query("pair") pair: String): Call<InfoPairData>
}