package ru.mykvm.cryptoneed.data.model
data class InfoPairData (
    var busd_sum: String?,
    var price_buy: String?,
    var price_sell: String?,
    var delta_buy: String?,
    var delta_sell: String?,
    var trade_mode: String?,
    var pair_status: String?
)
