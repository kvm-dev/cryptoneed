package ru.mykvm.cryptoneed.data.adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.pair_item.view.*
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.PairDetails
import ru.mykvm.cryptoneed.data.model.PairServices
import ru.mykvm.cryptoneed.ui.cabinet.Cabinet
import ru.mykvm.cryptoneed.ui.pair.PairSettingsFragment

class PairRecyclerAdapter(val userID: String, private val context: Context, private val pairList: MutableList<PairServices>):
    RecyclerView.Adapter<PairRecyclerAdapter.PairViewHolder>() {
    class PairViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val pairItem: TextView = itemView.pair_hint
        val pairStatus: TextView = itemView.pair_type
        private val buttonSettings: Button = itemView.settings
        private val pairData = "pairData"
        fun bind(listItem: PairServices, userID: String) {
            buttonSettings.setOnClickListener   {
                val context = itemView.context
                val activity : Cabinet = context as Cabinet
                var details = PairDetails(userID!!, itemView.pair_hint.text.toString()!!)
                val settingsFragment = PairSettingsFragment()
                val bundle = Bundle()
                bundle.putParcelable(pairData, details)
                settingsFragment.arguments = bundle
                val transaction = context.supportFragmentManager.beginTransaction()
                transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                transaction.replace(R.id.container, settingsFragment)
                transaction.commit()
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PairViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.pair_item, parent, false)
        return PairViewHolder(itemView)
    }
    override fun getItemCount() = pairList.size
    override fun onBindViewHolder(holder: PairViewHolder, position: Int) {
        val listItem = pairList[position]
        holder.bind(listItem, userID)
        if(pairList[position].pair?.contains("ADA") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_ada)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_ada, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("ALICE") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_alice)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_alice, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("ATOM") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_atom)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_atom, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("BNB") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_bnb)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bnb, 0, 0, 0)
        }
       else if(pairList[position].pair?.contains("BTC") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_btc)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_btc, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("CELR") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_celr)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_celr, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("DODO") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_dodo)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_dodo, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("DOGE") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_doge)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_doge, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("DOT") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_dot)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_dot, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("ETH") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_eth)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_eth, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("FOR") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_for)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_for, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("GALA") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_gala)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_gala, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("POND") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_pond)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pond, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("SOL") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_sol)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_sol, 0, 0, 0)
        }
        else if(pairList[position].pair?.contains("WIN") == true)
        {
            holder.pairItem.text = context.getString(R.string.pair_win)
            holder.pairItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_win, 0, 0, 0)
        }
        //check status of pair
        if(pairList[position].pair_status?.contains("0") == true)
        {
            holder.pairStatus.setTextColor(ContextCompat.getColor(context, R.color.pair_no_disabled))
            holder.pairStatus.text = context.getString(R.string.pair_is_disabled)
        }
        else if(pairList[position].pair_status?.contains("1") == true)
        {
            holder.pairStatus.setTextColor(ContextCompat.getColor(context, R.color.pair_no_active))
            holder.pairStatus.text = context.getString(R.string.pair_is_no_active)
        }
        else if(pairList[position].pair_status?.contains("2") == true)
        {
            holder.pairStatus.setTextColor(ContextCompat.getColor(context, R.color.pair_active))
            holder.pairStatus.text = context.getString(R.string.pair_is_active)
        }
    }
}

