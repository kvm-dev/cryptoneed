package ru.mykvm.cryptoneed.data.model.retrofit.interfaces

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ru.mykvm.cryptoneed.data.model.SimplyData

interface SendPairSettingsAPI {
    @GET("cryptoneed/pairsettingsupdate.php")
    fun updateSettings(@Query("pair") pair: String, @Query("busdSum") busdSum: Int, @Query("priceBuy") priceBuy: Double, @Query("priceSell") priceSell: Double, @Query("deltaBuy") deltaBuy: Int, @Query("deltaSell") deltaSell: Int, @Query("userID") userID: String, @Query("tradeMode") tradeMode: Int, @Query("pairStatus") pairStatus: Int): Call<SimplyData>
}