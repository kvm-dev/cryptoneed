package ru.mykvm.cryptoneed.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class PairDetails(val userID: String, val pairName: String) : Parcelable