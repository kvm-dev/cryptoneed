package ru.mykvm.cryptoneed.data

import ru.mykvm.cryptoneed.data.model.SimplyData

class RegRepository (val dataSource: RegistrationDataSource){
    // in-memory cache of the loggedInUser object
    var user: SimplyData? = null
        private set
    val isLoggedIn: Boolean
        get() = user != null
    init {
        user = null
    }
    fun logout() {
        user = null
        dataSource.logout()
    }
    fun reg(username: String, userPassword: String, apikey: String, secretkey: String, promo: String): Result<SimplyData> {
        // handle login
        val result = dataSource.registration(username, userPassword, apikey, secretkey, promo)
        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }
        return result
    }
    private fun setLoggedInUser(loggedInUser: SimplyData) {
        this.user = loggedInUser
    }
}