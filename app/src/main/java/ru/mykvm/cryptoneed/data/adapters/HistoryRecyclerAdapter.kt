package ru.mykvm.cryptoneed.data.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.history_item.view.*
import kotlinx.android.synthetic.main.history_item.view.pair_hint
import ru.mykvm.cryptoneed.R
import ru.mykvm.cryptoneed.data.model.HistoryServices

class HistoryRecyclerAdapter (private val context: Context, private val historyList: MutableList<HistoryServices>):
    RecyclerView.Adapter<HistoryRecyclerAdapter.HistoryViewHolder>() {
    class HistoryViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val pairHint: TextView = itemView.pair_hint
        val historyDate: TextView = itemView.history_date
        val historyType: TextView = itemView.history_type
        val historyCost: TextView = itemView.history_cost
        val historyQuantity: TextView = itemView.history_quantity
        val historyCommission: TextView = itemView.history_commission
        val historyExpense: TextView = itemView.history_expense
        val pairData = "pairData"
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.history_item, parent, false)
        return HistoryViewHolder(itemView)
    }
    override fun getItemCount() = historyList.size
    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        if(historyList[position].symbol?.contains("ADA") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_ada)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_ada, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("ALICE") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_alice)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_alice, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("ATOM") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_atom)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_atom, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("BNB") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_bnb)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bnb, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("BTC") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_btc)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_btc, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("CELR") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_celr)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_celr, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("DODO") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_dodo)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_dodo, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("DOGE") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_doge)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_doge, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("DOT") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_dot)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_dot, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("ETH") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_eth)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_eth, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("FOR") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_for)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_for, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("GALA") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_gala)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_gala, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("POND") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_pond)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pond, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("SOL") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_sol)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_sol, 0, 0, 0)
        }
        else if(historyList[position].symbol?.contains("WIN") == true)
        {
            holder.pairHint.text = context.getString(R.string.pair_win)
            holder.pairHint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_win, 0, 0, 0)
        }
        //check type of order
        if(historyList[position].isBuyer?.contains("true") == true)
        {
            holder.historyType.setTextColor(ContextCompat.getColor(context, R.color.pair_active))
            holder.historyType.text = context.getString(R.string.history_buy)
        }
        else if(historyList[position].isBuyer?.contains("false") == true)
        {
            holder.historyType.setTextColor(ContextCompat.getColor(context, R.color.pair_no_disabled))
            holder.historyType.text = context.getString(R.string.history_sell)
        }
        holder.historyDate.text = historyList[position].time
        holder.historyCost.text = historyList[position].price
        holder.historyQuantity.text = historyList[position].qty
        holder.historyCommission.text = historyList[position].commission
        holder.historyExpense.text = historyList[position].quoteQty
    }
}

