package ru.mykvm.cryptoneed.data

import ru.mykvm.cryptoneed.data.model.SimplyData

class PairRepository(val dataSource: PairDataSource) {
    // in-memory cache of the loggedInUser object
    var data: SimplyData? = null
        private set
    init {
        data = null
    }
    fun update(tradeQuantity: Int, manualBuyPrice: Double, manualSellPrice: Double, downDelta: Int, percentBenefit: Int): Result<SimplyData> {
        // data
        val result = dataSource.update(tradeQuantity, manualBuyPrice, manualSellPrice, downDelta, percentBenefit)
        if (result is Result.Success) {
            setVerifiedData(result.data)
        }
        return result
    }
    private fun setVerifiedData(verifiedData: SimplyData) {
        this.data = verifiedData
    }
}