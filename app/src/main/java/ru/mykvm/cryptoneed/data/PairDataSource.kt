package ru.mykvm.cryptoneed.data

import ru.mykvm.cryptoneed.data.model.SimplyData
import java.io.IOException

class PairDataSource{
    fun update(tradeQuantity: Int, manualBuyPrice: Double, manualSellPrice: Double, downDelta: Int, percentBenefit: Int): Result<SimplyData> {
        try {
            // TODO: handle loggedInUser authentication
            val success = SimplyData( "ok")
            return Result.Success(success)

        } catch (e: Throwable) {
            return Result.Error(IOException("Process Error", e))
        }
    }
}