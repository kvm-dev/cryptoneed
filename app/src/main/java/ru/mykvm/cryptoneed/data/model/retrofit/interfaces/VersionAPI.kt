package ru.mykvm.cryptoneed.data.model.retrofit.interfaces

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ru.mykvm.cryptoneed.data.model.SimplyData

interface VersionAPI {
    @GET("cryptoneed/version.php")
    fun getVersion(): Call<SimplyData>
}