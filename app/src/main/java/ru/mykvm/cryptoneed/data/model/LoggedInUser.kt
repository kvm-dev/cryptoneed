package ru.mykvm.cryptoneed.data.model

data class LoggedInUser(
    val userId: String,
    val displayName: String
)