package ru.mykvm.cryptoneed.data.model.retrofit.interfaces

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ru.mykvm.cryptoneed.data.model.HistoryServices
import ru.mykvm.cryptoneed.data.model.PairServices

interface RetrofitHistory {
    @GET("cryptoneed/history.php")
    fun getHistoryList(@Query("userid") sID: String, @Query("pair") pair: String): Call<MutableList<HistoryServices>>
}