package ru.mykvm.cryptoneed.data.model.retrofit.interfaces

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ru.mykvm.cryptoneed.data.model.SimplyData

interface SupportAPI {
    @GET("cryptoneed/support.php")
    fun sendToSupport(@Query("userID") userID: String, @Query("subject") subject: String, @Query("message") message: String): Call<SimplyData>
}