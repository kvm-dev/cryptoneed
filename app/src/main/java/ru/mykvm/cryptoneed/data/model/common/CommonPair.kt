package ru.mykvm.cryptoneed.data.model.common

import ru.mykvm.cryptoneed.data.model.retrofit.RetrofitClientPair
import ru.mykvm.cryptoneed.data.model.retrofit.interfaces.RetrofitPair

object CommonPair {
    private const val BASE_URL = "https://mykvm.ru/"
    val retrofitPair: RetrofitPair
        get() = RetrofitClientPair.getClientPair(BASE_URL).create(RetrofitPair::class.java)
}