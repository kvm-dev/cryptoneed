package ru.mykvm.cryptoneed.data.model.retrofit.interfaces

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ru.mykvm.cryptoneed.data.model.PairServices

interface RetrofitPair {
    @GET("cryptoneed/pairs.php")
    fun getPairList(@Query("userid") sID: String): Call<MutableList<PairServices>>
}