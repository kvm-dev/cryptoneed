package ru.mykvm.cryptoneed.data

import ru.mykvm.cryptoneed.data.model.SimplyData
import java.io.IOException

class RegistrationDataSource {
    fun registration(
        username: String,
        userPassword: String,
        apiKey: String,
        secretKey: String,
        promo: String,
    ): Result<SimplyData> {
        try {
            // TODO: handle loggedInUser authentication
            val success = SimplyData("ok")
            return Result.Success(success)

        } catch (e: Throwable) {
            return Result.Error(IOException("Registration Error", e))
        }
    }
    fun logout() {
        // TODO: revoke authentication
    }
}