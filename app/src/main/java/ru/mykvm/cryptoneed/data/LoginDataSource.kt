package ru.mykvm.cryptoneed.data

import ru.mykvm.cryptoneed.data.model.SimplyData
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {
    fun login(username: String, userpassword: String): Result<SimplyData> {
        try {
            // TODO: handle loggedInUser authentication
            val success = SimplyData( "ok")
            return Result.Success(success)

        } catch (e: Throwable) {
            return Result.Error(IOException("Process Error", e))
        }
    }
    fun logout() {
        // TODO: revoke authentication
    }
}