package ru.mykvm.cryptoneed.data.model.retrofit

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.mykvm.cryptoneed.data.model.retrofit.interfaces.PairSettingsAPI
import java.io.IOException

class PairSettingsRetrofitImpl {
    fun getRequest(): PairSettingsAPI {
        val podRetrofit = Retrofit.Builder()
            .baseUrl("https://mykvm.ru/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(createOkHttpClient(PODInterceptor()))
            .build()
        return podRetrofit.create(PairSettingsAPI::class.java)
    }
    private fun createOkHttpClient(interceptor: Interceptor): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(interceptor)
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpClient.addInterceptor(httpLoggingInterceptor.apply {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        })
        return httpClient.build()
    }
    inner class PODInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            return chain.proceed(chain.request())
        }
    }
}