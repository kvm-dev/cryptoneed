package ru.mykvm.cryptoneed

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import ru.mykvm.cryptoneed.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val mIsDynamic = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //check Connection
        if (isNetworkAvailable(applicationContext)) {
            // it's ok
        } else {
            Toast.makeText(this, getString(R.string.internet_error), Toast.LENGTH_LONG).show()
            finish()
        }
    }
    //make not deprecated method at the future
    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo!!
            .isConnected
    }
    //override hard backbutton
    override fun onBackPressed() {
        AlertDialog.Builder(this).apply {
            setTitle(getString(R.string.exit_confirm))
            setMessage(getString(R.string.exit_question))

            setPositiveButton(getString(R.string.exit_yes)) { _, _ ->
                super.onBackPressed()
            }
            setNegativeButton(getString(R.string.exit_no)) { _, _ ->
                // if user press no, then return the activity
                Toast.makeText(
                    this@MainActivity, getString(R.string.exit_huh),
                    Toast.LENGTH_LONG
                ).show()
            }
            setCancelable(true)
        }.create().show()
    }


}
